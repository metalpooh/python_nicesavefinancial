# -*- coding: utf-8 -*-
__author__ = 'ByungJoo'

from bs4 import BeautifulSoup
import urllib2
import csv
import sys
import time


reload(sys)
sys.setdefaultencoding('utf-8')


jongname = []
shcode = []


def get_CSV():
    with open('StockMaster0917_code.csv','rb') as csvfile:
    #with open('StockMasterTest.csv','rb') as csvfile:
        csvReader = csv.reader(csvfile,delimiter = ',', quotechar = '|' )
        for row in csvReader:
            #name = row[0].decode('cp949')
            code = row[0][1:]
            #jongname.append(name)
            shcode.append(code)

    #getFinancialValue()
    getTestValue()

def get_html(code):
    sec = 0.2
    financialName = 'NiceFinancial'+code+'.html'
    with open(financialName,'r') as htmlFile:
        html = htmlFile.read()
        return html


def get_financialHTML(code):
    html = get_html(code)

    soup = BeautifulSoup(html, "html.parser")
    # 재무정보 중 연결을 가지고 오기 위해서
    financial = soup.find('div', id = 'Fin0')
    financialHTML = str(financial)

    return financialHTML

def getTestValue():
    cnt = 0
    for code in shcode:

        cnt = cnt + 1

        html = get_financialHTML(code)

        soup = BeautifulSoup(html, "html.parser")

        tables = soup.select('.list_b1')
        try:
            finSet = tables[0].thead.find_all('tr', recursive=False)
        except:
            finSet = []
        try:
            fin01 = tables[0].tbody.find_all('tr', recursive=False)
        except:
            fin01 = []



        setYearExtract = 0
        for tr in finSet:
            finSetTd = tr.find_all('th', recursive=False)

            cnt = 0
            setYearCnt = 0
            setYearSecondCnt = 0
            for year in finSetTd:

                if year.text == '2014.12.31':
                    setYearCnt = cnt-1

                elif year.text == '2014.03.31':
                    setYearSecondCnt = cnt-1

                cnt = cnt + 1

            if setYearCnt == 0:
                setYearCnt = setYearSecondCnt

            if setYearCnt != -1:
                setYearExtract = setYearCnt
            else :
                setYearExtract = 0




        print code
        saveTestValue(fin01,setYearExtract,code)

shcodeWrite = [] # 종목코드

#유동비율

#당좌비율

#부채비율

#매출채권회전율

#매입채권 회전율


test1 = []
test2 = []
test3 = []
test4 = []
test5 = []


def saveTestValue(trs,setYearExtract,code):

    test1true = 0
    test2true = 0
    test3true = 0
    test4true = 0
    test5true = 0


    numCheck = 0;
    shcodeWrite.append(code)

    for tr in trs:

        ths = tr.find_all('th', recursive=False)
        tds = tr.find_all('td', recursive=False)
        try:
            nameText = ths[0].text.encode('utf-8')
        except :
            nameText = "--"
            test1.append("-")
            test2.append("-")
            test3.append("-")
            test4.append("-")
            test5.append("-")
            test1true = 1
            test2true = 1
            test3true = 1
            test4true = 1
            test5true = 1

        numCheck = numCheck + 1

        if nameText == '유동비율' :
            getText = getSetData(tds,setYearExtract)
            test1.append(getText)
            test1true = 1

        if nameText == '당좌비율' :
            getText = getSetData(tds,setYearExtract)
            test2.append(getText)
            test2true = 1

        if nameText == '부채비율' :
            getText = getSetData(tds,setYearExtract)
            test3.append(getText)
            test3true = 1

#----------------------------------------------------

        if nameText == '매출채권회전율' :
            getText = getSetData(tds,setYearExtract)
            test4.append(getText)
            test4true = 1

        if nameText == '매입채무회전율' :
            getText = getSetData(tds,setYearExtract)
            test5.append(getText)
            test5true = 1

#-------------------------------------------------------



    if test1true == 0 :
        test1.append("--")

    if test2true == 0 :
        test2.append("--")

    if test3true == 0 :
        test3.append("--")

    if test4true == 0 :
        test4.append("--")

    if test5true == 0 :
        test5.append("--")


    if code == '950130':


        with open("output.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            #writer.writerow(["aa","총자본순이익율","자기자본순이익율","매출액순이익율","매출액영업이익율","금융비용/매출액비율"])
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],test1[plusChange],test2[plusChange],test3[plusChange],test4[plusChange],test5[plusChange]])
                plusChange = plusChange + 1


def getFinancialValue():
    cnt = 0
    for code in shcode:
        print str(cnt) + ':' + str(code)

        if cnt == 666:
            time.sleep(5)
        elif cnt == 1200:
            time.sleep(5)
        elif cnt == 1600:
            time.sleep(5)


        cnt = cnt + 1

        html = get_financialHTML(code)
        soup = BeautifulSoup(html, "html.parser")

        tables = soup.select('.list_b1')
        try:
            finSet = tables[0].thead.find_all('tr', recursive=False)
        except:
            finSet = []
        try:
            fin01 = tables[0].tbody.find_all('tr', recursive=False)
        except:
            fin01 = []



        setYearExtract = 0
        for tr in finSet:
            finSetTd = tr.find_all('th', recursive=False)
            setYear = finSetTd[1].text
            setYearSecond = finSetTd[2].text
            if setYear != "":
                setYearExtract = setYear[3]
            elif setYearSecond == "":
                setYearExtract = '5'
            else:
                setYear = finSetTd[2].text
                if setYear[3] == '1':
                    setYearExtract = '3'
                elif setYear[3] == '2':
                    setYearExtract = '4'
                else :
                    setYearExtract = '5'





        saveFinancialData(fin01,setYearExtract,code)

shcodeWrite = [] # 종목코드

Earning1 = []  # 총자본 수익률
Earning2 = []  # 자기자본순이익율
Earning3 = []  # 매출액순이익율
Earning4 = []  # 매출액영업이익율
Earning5 = []  # 금융비용/매출액비율

Ratio1 = [] # 총자산증가율
Ratio2 = [] # 자기자본증가율
Ratio3 = [] # 매출액증가율
Ratio4 = [] # 영업이익증가율
Ratio5 = [] # 법인세비용차감전순이익증가율
Ratio6 = [] # 순이익증가율

Safety1 = [] #영업활동현금흐름이자보상비율
Safety2 = [] #영업활동현금흐름이자보상비율
Safety3 = [] #영업이익이자보상비율
Safety4 = [] #법인세차감전순이익이자보상비율
Safety5 = [] #자기자본비율
Safety6 = [] #유동비율

Safety7 = [] #당좌비율
Safety8 = [] #비유동자산비율
Safety9 = [] #비유동자산장기적합율
Safety10 = [] #부채비율
Safety11 = [] #유동부채비율
Safety12 = [] #차입금의존도
Safety13 = [] #차입금/자기자본

turnover1 = [] #총자본회전율
turnover2 = [] #재고자산회전율1
turnover3 = [] #매출채권회전율
turnover4 = [] #매입채무회전율

assett1 = [] #총차입금
assett2 = [] #단기차입금
assett3 = [] #유동성장기부채
assett4 = [] #장기차입금


def saveFinancialData(trs,setYearExtract,code):

    Earning1true = 0
    Earning2true = 0
    Earning3true = 0
    Earning4true = 0
    Earning5true = 0

    Ratio1true = 0
    Ratio2true = 0
    Ratio3true = 0
    Ratio4true = 0
    Ratio5true = 0
    Ratio6true = 0

    Safety1true = 0
    Safety2true = 0
    Safety3true = 0
    Safety4true = 0
    Safety5true = 0
    Safety6true = 0
    Safety7true = 0
    Safety8true = 0
    Safety9true = 0
    Safety10true = 0
    Safety11true = 0
    Safety12true = 0
    Safety13true = 0

    turnover1true = 0
    turnover2true = 0
    turnover3true = 0
    turnover4true = 0

    assett1true = 0
    assett2true = 0
    assett3true = 0
    assett4true = 0

    numCheck = 0;
    shcodeWrite.append(code)

    for tr in trs:

        ths = tr.find_all('th', recursive=False)
        tds = tr.find_all('td', recursive=False)
        try:
            nameText = ths[0].text.encode('utf-8')
        except :
            nameText = "--"
            Earning1.append("nil")  # 총자본 수익률
            Earning2.append("nil")  # 자기자본순이익율
            Earning3.append("nil")  # 매출액순이익율
            Earning4.append("nil")  # 매출액영업이익율
            Earning5.append("nil") # 금융비용/매출액비율

            Ratio1.append("nil") # 총자산증가율
            Ratio2.append("nil") # 자기자본증가율
            Ratio3.append("nil") # 매출액증가율
            Ratio4.append("nil") # 영업이익증가율
            Ratio5.append("nil") # 법인세비용차감전순이익증가율
            Ratio6.append("nil") # 순이익증가율

            Safety1.append("nil") #영업활동현금흐름이자보상비율
            Safety2.append("nil") #영업활동현금흐름이자보상비율
            Safety3.append("nil") #영업이익이자보상비율
            Safety4.append("nil") #법인세차감전순이익이자보상비율
            Safety5.append("nil") #자기자본비율
            Safety6.append("nil") #유동비율
            Safety7.append("nil") #당좌비율
            Safety8.append("nil") #비유동자산비율
            Safety9.append("nil") #비유동자산장기적합율
            Safety10.append("nil") #부채비율
            Safety11.append("nil") #유동부채비율
            Safety12.append("nil") #차입금의존도
            Safety13.append("nil") #차입금/자기자본

            turnover1.append("nil") #총자본회전율
            turnover2.append("nil") #재고자산회전율1
            turnover3.append("nil") #매출채권회전율
            turnover4.append("nil") #매입채무회전율

            assett1.append("nil") #총차입금
            assett2.append("nil") #단기차입금
            assett3.append("nil") #유동성장기부채
            assett4.append("nil") #장기차입금

        numCheck = numCheck + 1



        if nameText == '총자본순이익율' :
            getText = getSetData(tds,setYearExtract)
            Earning1.append(getText)
            Earning1true = 1

        if nameText == '자기자본순이익율' :
            getText = getSetData(tds,setYearExtract)
            Earning2.append(getText)
            Earning2true = 1

        if nameText == '매출액순이익율' :
            getText = getSetData(tds,setYearExtract)
            Earning3.append(getText)
            Earning3true = 1

        if nameText == '매출액영업이익율' :
            getText = getSetData(tds,setYearExtract)
            Earning4.append(getText)
            Earning4true = 1

        if nameText == '금융비용/매출액비율' :
            getText = getSetData(tds,setYearExtract)
            Earning5.append(getText)
            Earning5true = 1
    #---------------------------------------------
        if nameText == '총자산증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio1.append(getText)
            Ratio1true = 1

        if nameText == '자기자본증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio2.append(getText)
            Ratio2true = 1

        if nameText == '매출액증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio3.append(getText)
            Ratio3true = 1

        if nameText == '영업이익증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio4.append(getText)
            Ratio4true = 1

        if nameText == '법인세비용차감전순이익증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio5.append(getText)
            Ratio5true = 1

        if nameText == '순이익증가율' :
            getText = getSetData(tds,setYearExtract)
            Ratio6.append(getText)
            Ratio6true = 1

    #---------------------------------------------
        if nameText == '영업활동현금흐름이자보상비율' :
            getText = getSetData(tds,setYearExtract)
            Safety1.append(getText)
            Safety1true = 1

        if nameText == '영업활동현금흐름이자보상비율' :
            getText = getSetData(tds,setYearExtract)
            Safety2.append(getText)
            Safety2true = 1

        if nameText == '영업이익이자보상비율' :
            getText = getSetData(tds,setYearExtract)
            Safety3.append(getText)
            Safety3true = 1

        if nameText == '법인세차감전순이익이자보상비율' :
            getText = getSetData(tds,setYearExtract)
            Safety4.append(getText)
            Safety4true = 1

        if nameText == '자기자본비율' :
            getText = getSetData(tds,setYearExtract)
            Safety5.append(getText)
            Safety5true = 1

        if nameText == '유동비율' :
            getText = getSetData(tds,setYearExtract)
            Safety6.append(getText)
            Safety6true = 1

        if nameText == '당좌비율' :
            getText = getSetData(tds,setYearExtract)
            Safety7.append(getText)
            Safety7true = 1

        if nameText == '비유동자산비율' :
            getText = getSetData(tds,setYearExtract)
            Safety8.append(getText)
            Safety8true = 1

        if nameText == '비유동자산장기적합율' :
            getText = getSetData(tds,setYearExtract)
            Safety9.append(getText)
            Safety9true = 1

        if nameText == '부채비율' :
            getText = getSetData(tds,setYearExtract)
            Safety10.append(getText)
            Safety10true = 1

        if nameText == '유동부채비율' :
            getText = getSetData(tds,setYearExtract)
            Safety11.append(getText)
            Safety11true = 1

        if nameText == '차입금의존도' :
            getText = getSetData(tds,setYearExtract)
            Safety12.append(getText)
            Safety12true = 1

        if nameText == '차입금/자기자본' :
            getText = getSetData(tds,setYearExtract)
            Safety13.append(getText)
            Safety13true = 1

#----------------------------------------------------
        if nameText == '총자본회전율' :
            getText = getSetData(tds,setYearExtract)
            turnover1.append(getText)
            turnover1true = 1

        if nameText == '재고자산회전율1' :
            getText = getSetData(tds,setYearExtract)
            turnover2.append(getText)
            turnover2true = 1

        if nameText == '매출채권회전율' :
            getText = getSetData(tds,setYearExtract)
            turnover3.append(getText)
            turnover3true = 1

        if nameText == '매입채무회전율' :
            getText = getSetData(tds,setYearExtract)
            turnover4.append(getText)
            turnover4true = 1

#-------------------------------------------------------
        if nameText == '총차입금' :
            getText = getSetData(tds,setYearExtract)
            assett1.append(getText)
            assett1true = 1

        if nameText == '단기차입금' :
            getText = getSetData(tds,setYearExtract)
            assett2.append(getText)
            assett2true = 1

        if nameText == '유동성장기부채' :
            getText = getSetData(tds,setYearExtract)
            assett3.append(getText)
            assett3true = 1

        if nameText == '장기차입금' :
            getText = getSetData(tds,setYearExtract)
            assett4.append(getText)
            assett4true = 1



    if Earning1true == 0 :
        Earning1.append("nilnil")

    if Earning2true == 0 :
        Earning2.append("nilnil")

    if Earning3true == 0 :
        Earning3.append("nilnil")

    if Earning4true == 0 :
        Earning4.append("nilnil")

    if Earning5true == 0 :
        Earning5.append("nilnil")

    if Ratio1true == 0 :
        Ratio1.append("nilnil")

    if Ratio2true == 0 :
        Ratio2.append("nilnil")

    if Ratio3true == 0 :
        Ratio3.append("nilnil")

    if Ratio4true == 0 :
        Ratio4.append("nilnil")

    if Ratio5true == 0 :
        Ratio5.append("nilnil")

    if Ratio6true == 0 :
        Ratio6.append("nilnil")

    if Safety1true == 0 :
        Safety1.append("nilnil")

    if Safety2true == 0 :
        Safety2.append("nilnil")

    if Safety3true == 0 :
        Safety3.append("nilnil")

    if Safety4true == 0 :
        Safety4.append("nilnil")

    if Safety5true == 0 :
        Safety5.append("nilnil")

    if Safety6true == 0 :
        Safety6.append("nilnil")

    if Safety7true == 0 :
        Safety7.append("nilnil")

    if Safety8true == 0 :
        Safety8.append("nilnil")

    if Safety9true == 0 :
        Safety9.append("nilnil")

    if Safety10true == 0 :
        Safety10.append("nilnil")

    if Safety11true == 0 :
        Safety11.append("nilnil")

    if Safety12true == 0 :
        Safety12.append("nilnil")

    if Safety13true == 0 :
        Safety13.append("nilnil")

    if turnover1true == 0 :
        turnover1.append("nilnil")

    if turnover2true == 0 :
        turnover2.append("nilnil")

    if turnover3true == 0 :
        turnover3.append("nilnil")

    if turnover4true == 0 :
        turnover4.append("nilnil")

    if assett1true == 0 :
        assett1.append("nilnil")

    if assett2true == 0 :
        assett2.append("nilnil")

    if assett3true == 0 :
        assett3.append("nilnil")

    if assett4true == 0 :
        assett4.append("nilnil")


    if code == '104480':
        print shcodeWrite
        print Safety6
        with open("output.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            #writer.writerow(["aa","총자본순이익율","자기자본순이익율","매출액순이익율","매출액영업이익율","금융비용/매출액비율"])
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],Earning1[plusChange],Earning2[plusChange],Earning3[plusChange],Earning4[plusChange],Earning5[plusChange]])
                plusChange = plusChange + 1

        with open("output2.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],Ratio1[plusChange],Ratio2[plusChange],Ratio3[plusChange],Ratio4[plusChange],Ratio5[plusChange],Ratio6[plusChange]])
                plusChange = plusChange + 1

        with open("output3.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],Safety1[plusChange],Safety2[plusChange],Safety3[plusChange],Safety4[plusChange],Safety5[plusChange],Safety6[plusChange]])
                plusChange = plusChange + 1

        with open("output4.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],Safety7[plusChange],Safety8[plusChange],Safety9[plusChange],Safety10[plusChange],Safety11[plusChange],Safety12[plusChange],Safety13[plusChange]])
                plusChange = plusChange + 1

        with open("output5.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',', quotechar='|')
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],turnover1[plusChange],turnover2[plusChange],turnover3[plusChange],turnover4[plusChange]])
                plusChange = plusChange + 1

        with open("output6.csv", "wb") as f:
            writer = csv.writer(f)
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],assett1[plusChange],assett2[plusChange],assett3[plusChange],assett4[plusChange]])
                plusChange = plusChange + 1






def getSetData(tds,setYear):




     return tds[setYear].text


if __name__ == '__main__':
    get_CSV()
